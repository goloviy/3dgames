﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayNode : Node
{
    [SerializeField] private Transform root;
    [SerializeField] private Animator animator;
    [SerializeField] private int speed = 0;
    
    public override NodeState Evalute()
    {
        var player = FindObjectOfType<PlayerController>();
        if (player == null)
        {
            animator.SetInteger("Movement",speed);
            root.Translate(Vector3.zero);
        }
        return NodeState.Success;
    }
}
