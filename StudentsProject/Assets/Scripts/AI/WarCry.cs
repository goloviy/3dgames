﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarCry : Node
{
    [SerializeField] private Animator animator;
    
    private enum mystate
    {
        Wait,
        WarCry,
        Complete,
        
    }

    private mystate state;
    private Coroutine warCryCoroutine;
    
    public override NodeState Evalute()
    {
        switch (state)
        {
            case mystate.Wait:
                state = mystate.WarCry;
                warCryCoroutine = StartCoroutine(WaitWarCry());
;                return NodeState.Running;
            case mystate.WarCry:
                if (warCryCoroutine != null)
                {
                    return NodeState.Running;
                }
                else
                {
                    state = mystate.Complete;
                    return NodeState.Success;
                }
            
            case mystate.Complete:
                return NodeState.Failure;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private IEnumerator WaitWarCry()
    {
        animator.SetTrigger("WarCry");
        yield return new WaitForSeconds(1.5f);
        warCryCoroutine = null;
    }
}
