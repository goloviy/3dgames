﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : Node
{
    [SerializeField] protected Node[] nodes;
    
    public override NodeState Evalute()
    {
        var anyChildRunnig = false;

        foreach (var node in nodes)
        {
            switch (node.Evalute())
            {
                   case NodeState.Success:
                       continue;
                   case NodeState.Failure:
                       NodeState = NodeState.Failure;
                       return NodeState;
                   case NodeState.Running:
                       anyChildRunnig = true;
                       continue;
                   default: 
                       throw new  ArgumentOutOfRangeException();
            }
        }

        NodeState = anyChildRunnig ? NodeState.Running : NodeState.Success;
        return NodeState;
    }
}
