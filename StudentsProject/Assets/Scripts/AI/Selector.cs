﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : Node
{
    [SerializeField] protected Node[] nodes;
    
    public override NodeState Evalute()
    {
        foreach (var node in nodes)
        {
            switch (node.Evalute())
            {
                case NodeState.Success:
                    NodeState = NodeState.Success;
                    return NodeState;
                case NodeState.Failure:
                    continue;
                case NodeState.Running:
                    NodeState = NodeState.Running;
                    return NodeState;
                default: 
                    throw new  ArgumentOutOfRangeException();
            }
        }
        NodeState = NodeState.Failure;
        return NodeState;
    }
}
