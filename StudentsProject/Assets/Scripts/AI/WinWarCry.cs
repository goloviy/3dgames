﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinWarCry : Node
{
    [SerializeField] private Animator animator;
    
    private enum mystate
    {
        Wait,
        WarCry,
        Complete,
        
    }
    private mystate state;
    private Coroutine warCryCoroutine;
    
    public override NodeState Evalute()
    {
        return NodeState;
    }
}
