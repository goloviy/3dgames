﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{    
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
    
}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    private CharacterState characterState;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform firePoint;
    [SerializeField] private Transform skillPoint;
    [SerializeField] private GameObject deadShotEffect;
    

    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    private static readonly int SkillTrigger = Animator.StringToHash("Skill");

    public void AttackEvent()
    {
        var obj = ObjectsPool.Instance.GetObject(ballPrefab);
        obj.transform.position = firePoint.transform.position;
        obj.transform.rotation = firePoint.transform.rotation;
        
        var rig = obj.GetComponent<Rigidbody>();
        if (rig != null)
        {  
            rig.velocity = Vector3.zero;
            rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
        }
    }

    public void SkillEvent()
    {
        StartCoroutine(SkillCoroutine());        
    }

    public IEnumerator SkillCoroutine()
    {
        for (int i = 0; i < 5; i++)
        {
            var obj = Instantiate(ballPrefab, firePoint.position, Quaternion.identity);
            var rig = obj.GetComponent<Rigidbody>();            
            
            

            if (rig != null)
            {
                rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
            }
            yield return new WaitForSeconds(0.1f);
        } 
    }
    
    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

   
    private void Start()
    {
        characterState = CharacterState.Idle;
        InputController.OnInputAction += OnInputCommand;  
        
        ObjectsPool.Instance.PrepareObject(ballPrefab,10);
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    private void OnInputCommand(InputCommand command)
    {
        switch (command)
        {
            case InputCommand.Fire:                
                Attack();
                break;
            case InputCommand.Skill:                
                Skill();
                break;
            case InputCommand.DeadShot:
                DeadShoot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(command), command, null);
        }
    }

    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(AttackTrigger);
        characterState = CharacterState.Attack;
        
        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 0.5f, gameObject);
    }

    private void Skill()
    {       
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        
        animator.SetTrigger(SkillTrigger); 
        characterState = CharacterState.Skill;
        
        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 1f, gameObject);
    }

    private void DeadShoot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        
        animator.SetTrigger(SkillTrigger);
        DelayRun.Execute(delegate { characterState = CharacterState.Idle;}, 1.65f, gameObject);
        DelayRun.Execute(DeadShotExecute, 1.35f, gameObject);
    }

    private void DeadShotExecute()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(skillPoint.position, skillPoint.forward, 100f);
        if (hits == null)
        {
            return;
        }

        var obj = Instantiate(deadShotEffect, skillPoint.position, skillPoint.rotation);
        Destroy(obj, 1f);
        
        var healthObjects = new List<Health>();
        foreach (var hit in hits)
        {
            var health = hit.transform.GetComponent<Health>();
            if (health != null)
            {
                healthObjects.Add(health);
                if (healthObjects.Count == 3)
                {
                    break;
                }
            }
        }

        var timer = 0f;
        foreach (var healthObject in healthObjects)
        {
            DelayRun.Execute(delegate {healthObject.SetDamage(int.MaxValue);},timer, gameObject);
            timer += 0.2f;
        }
    }
}
