﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusMushrooms : Bonus
{
    [SerializeField] private int mushrooms = 1;
    private Time timer;

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void SetBonus()
    {
       base.SetBonus();
        Debug.Log($"Added {mushrooms} gold");
    }
}
