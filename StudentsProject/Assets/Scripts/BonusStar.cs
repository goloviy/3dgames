﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusStar : Bonus
{
    [SerializeField] private int star = 1;
    protected override void SetBonus()
    {
        base.SetBonus();
        Debug.Log($"Added {star} star");
        GameContoroller.Star += star;
    }
}
