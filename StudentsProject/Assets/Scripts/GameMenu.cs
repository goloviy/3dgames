﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;

    [SerializeField] private GameObject pausePanel;

    [SerializeField] private Button pauseButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Button resumButton;
    
    // Start is called before the first frame update
    void Start()
    {
        mainPanel.SetActive(true);
        pausePanel.SetActive(false);
        
        exitButton.onClick.AddListener(onExitButton);
        pauseButton.onClick.AddListener(delegate { OnPause(true); });
        resumButton.onClick.AddListener(delegate { OnPause(false); });
    }

    private void OnPause(bool isPause)
    {
        Time.timeScale = isPause ? 0f : 1f;
        
        mainPanel.SetActive(!isPause);
        pausePanel.SetActive(isPause);
    }

    private void onExitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }

    
}
