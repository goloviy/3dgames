﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int health = 1;
    [SerializeField] private GameObject[] healthObject;

    public event Action onDieAction;
    void Start()
    {
        SetupHealthObject();
    }

    public virtual void SetDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
            return;
        }
        SetupHealthObject();
    }

    private void SetupHealthObject()
    {
        var nm = Mathf.Clamp(health - 1, 0, healthObject.Length);
        for (int i = 0; i < healthObject.Length; i++)
        {
            healthObject[i].SetActive(i == nm);
        }
    }

    private void Die()
    {
        Destroy(gameObject);
        onDieAction?.Invoke();
    }
}
