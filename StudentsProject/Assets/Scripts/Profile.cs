﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Profile 
{
    [System.Serializable]
    private class MainData
    {
        public List<int> LevelStars;
        public int Money = 10;
    }
    
    [System.Serializable]
    private class PlayerData
    {
        public bool Sound = true;
        public bool Music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckMainData()
    {
        Debug.Log("Init Profile main data");
        if (mainData != null)
        {
            return;
        }

        if (!PlayerPrefs.HasKey("MainData"))
        {
            mainData = new MainData();
            PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
            return;
        }

        mainData = JsonUtility.FromJson<MainData>(PlayerPrefs.GetString("MainData"));
    }
    
    private static void CheckPlayer()
    {
        Debug.Log("Init Profile player data");
        if (playerData != null)
        {
            return;
        }

        if (!PlayerPrefs.HasKey("PlayerData"))
        {
            playerData = new PlayerData();
            PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
            return;
        }

        playerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PlayerData"));
    }

    public static void Save(bool main = true, bool player = true)
    {
        if (main)
        {
            PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
        }

        if (player)
        {
            PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
        }
    }

    public static int Money
    {
        get => mainData.Money;
        set
        {
            mainData.Money = value;
            if (mainData.Money < 0)
            {
                mainData.Money = 0;
            }
            
            Save(player:false);
        }
    }

    public static int GetOpenedLevelCount => mainData.LevelStars.Count;

    public static int GetLevelStars(int Level)
    {
        if (Level >= mainData.LevelStars.Count)
        {
            return -1;
        }

        return mainData.LevelStars[Level];
    }

    public static void SetLevelStars(int level, int stars)
    {
        if (level > mainData.LevelStars.Count)
        {
            Debug.LogError($"Level {level} is not opened");
        }

        if (level == mainData.LevelStars.Count)
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.LevelStars.Add(stars);
            Save(player:false);
            return;
        }

        if (stars > mainData.LevelStars[level])
        {
            mainData.LevelStars[level] = stars;
            Save(player:false);
        }
    }
}
