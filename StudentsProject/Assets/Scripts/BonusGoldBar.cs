﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class BonusGoldBar : MonoBehaviour
{
    [SerializeField] private GameObject goldCoin;
    
    protected void OnMouseDown()
    {
        SetBonus();
        Destroy(gameObject);
    }

    private void SetBonus()
    {
        int numberGoldCoin = Random.Range(1, 4);
        for (int i = 0; i < numberGoldCoin; i++)
        {
            Instantiate(goldCoin, new Vector3(transform.position.x+i,transform.position.y,transform.position.z), Quaternion.identity);
        }
    }
}
