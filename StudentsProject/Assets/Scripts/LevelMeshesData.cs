﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "Levels/Data", order = 1)]
public class LevelMeshesData : ScriptableObject
{
    [SerializeField] private Serializablemeshinfo[] meshInfo;
    public Serializablemeshinfo[] MeshInfo => meshInfo;

    public void SetupData(GameObject obj)
    {
        var data = new List<Serializablemeshinfo>();
        foreach (var renderer in obj.transform.GetComponentsInChildren<MeshRenderer>())
        {
            var sharedMaterial = renderer.sharedMaterial;
            var target = renderer.gameObject;
            var sharedMesh = target.gameObject.GetComponent<MeshFilter>().sharedMesh;
            
            var meshInfo = new Serializablemeshinfo(obj.name,sharedMesh,sharedMaterial);
            data.Add(meshInfo);
        }

        meshInfo = data.ToArray();
    }
}
