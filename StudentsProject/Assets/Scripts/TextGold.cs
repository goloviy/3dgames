﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextGold : MonoBehaviour
{
    [SerializeField] private Text goldText;
    [SerializeField] private Text starText;

    
    void Start()
    {
        printText();
    }

    private void Update()
    {
        printText();
    }

    private void printText()
    {
        goldText.text = $"Количество золота: {GameContoroller.Gold}";
        starText.text = $"Stars: {GameContoroller.Star}";
    }
}
