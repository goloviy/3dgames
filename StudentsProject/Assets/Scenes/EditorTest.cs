﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TestPoint))]

public class EditorTest : Editor
{
    TestPoint _target;

    void OnEnable()
    {
        _target = (TestPoint) target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }

    private void OnSceneGUI()
    {
        Handles.color = Handles.xAxisColor;

        for (int i = 0; i < _target.interactionPoints.Length; i++)
        {
            Handles.SphereHandleCap(i, _target.interactionPoints[i], Quaternion.identity, 0.2f, EventType.Repaint);
        }


        for (int i = 0; i < _target.interactionPoints.Length; i++)
        {
            _target.interactionPoints[i] = Handles.PositionHandle(_target.interactionPoints[i], Quaternion.identity);
        }
    }
}